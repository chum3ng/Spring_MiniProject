package com.example.demo.model;

import org.hibernate.validator.constraints.NotEmpty;

public class Article {
    private int id;
    @NotEmpty
    private String title;
    @NotEmpty
    private String description;
    private String createDate;
    @NotEmpty
    private String author;
    private String thumbnail;

    public Article(int id, String title, String description, String createDate, String author, String thumbnail) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.createDate = createDate;
        this.author = author;
        this.thumbnail = thumbnail;

    }

    public Article() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }


    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", createDate='" + createDate + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}
