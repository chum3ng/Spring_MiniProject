package com.example.demo.controller;

import com.example.demo.model.Article;
import com.example.demo.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.UUID;

@Controller
@PropertySource("classpath:ams.properties")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @Value("${file.server.path}")
    private String serverPath;

    @GetMapping("/article")
    public String article(Model model){
        model.addAttribute("articles", articleService.findAll());
        return "article";
    }

    @GetMapping("/article/{id}") //@RequestParam int id
    public String articleOne(@PathVariable("id") int id, Model model){
        model.addAttribute("article", articleService.findOne(id));
        return "article_detail";
    }

    @GetMapping("/add")
    public String addArticle(Model model){
        model.addAttribute("formAdd", true);
        model.addAttribute("article", new Article());
        return "add";
    }

    @PostMapping("/add")
    public String saveArticle(@RequestParam("image") MultipartFile file, @Valid @ModelAttribute Article article, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors() || file.isEmpty()){
            model.addAttribute("formAdd", true);
            return "add";
        }else{
            String fileName = file.getOriginalFilename();
            String extension = fileName.substring(fileName.lastIndexOf('.') + 1);
            fileName = UUID.randomUUID() + "." + extension;
            try {
                Files.copy(file.getInputStream(), Paths.get(serverPath, fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
            article.setCreateDate(new Date().toString());
            article.setThumbnail("/image/" + fileName);
            articleService.insert(article);
            return "redirect:/article";   //back to article page with redirect change form post to get
        }
        //System.out.println(article);
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model){
        model.addAttribute("formAdd", false);
        model.addAttribute("article", articleService.findOne(id));
        return "add";
    }

    @PostMapping("/edit")
    public String saveEdit(@Valid @ModelAttribute Article article, BindingResult bindingResult, Model model, @RequestParam("image") MultipartFile file){
        if(bindingResult.hasErrors()){
            model.addAttribute("formAdd", false);
            return "add";
        }else{
            String fileName = file.getOriginalFilename();
            String extension = fileName.substring(fileName.lastIndexOf('.') + 1);
            fileName = UUID.randomUUID() + "." + extension;
            try {
                Files.copy(file.getInputStream(), Paths.get(serverPath, fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(fileName);
            article.setThumbnail("/image/" + fileName);

            article.setCreateDate(articleService.findOne(article.getId()).getCreateDate());
            articleService.edit(article);
            return "redirect:/article";
        }
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable int id){
        articleService.delete(id);
        return "redirect:/article";
    }

}
