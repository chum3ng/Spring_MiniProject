package com.example.demo.repository;

import com.example.demo.model.Article;

import java.util.List;

public interface ArticleRepository {

    void insert(Article article);
    Article findOne(int id);
    List<Article> findAll();
    void delete(int id);
    void edit(Article article);


}
