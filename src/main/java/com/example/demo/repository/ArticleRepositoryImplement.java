package com.example.demo.repository;

import com.example.demo.model.Article;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class ArticleRepositoryImplement implements ArticleRepository {

    private List<Article> articles = new ArrayList<>();

    public ArticleRepositoryImplement(){
        Faker faker = new Faker();
        for (int i = 1; i <= 10; i++)
            articles.add(new Article(i, faker.book().title(), faker.book().title(), new Date().toString(), faker.book().author(), faker.internet().image(100, 100, false, null)));
    }

    @Override
    public void insert(Article article) {
        articles.add(article);
    }

    @Override
    public Article findOne(int id) {
        for (Article a : articles){
            if (a.getId() == id) return a;
        }
        return null;
    }

    @Override
    public List<Article> findAll() {
        return articles;
    }

    @Override
    public void delete(int id) {
        for (int i = 0; i < articles.size(); i++){
            if (articles.get(i).getId() == id){
                articles.remove(i);
            }
        }
    }

    @Override
    public void edit(Article article) {
        for (int i = 0; i < articles.size(); i++){
            if(articles.get(i).getId() == article.getId()){
                articles.set(i, article);
            }
        }
    }
}
