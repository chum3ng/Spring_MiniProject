package com.example.demo.service;

import com.example.demo.model.Article;

import java.util.List;

public interface ArticleService {

    void insert(Article article);
    Article findOne(int id);
    List<Article> findAll();
    void delete(int id);
    void edit(Article article);

}
