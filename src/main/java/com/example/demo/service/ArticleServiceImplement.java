package com.example.demo.service;

import com.example.demo.model.Article;
import com.example.demo.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImplement implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Override
    public void insert(Article article) {
        articleRepository.insert(article);
    }

    @Override
    public Article findOne(int id) {
        return articleRepository.findOne(id);
    }

    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public void delete(int id) {
        articleRepository.delete(id);
    }

    @Override
    public void edit(Article article) {
        articleRepository.edit(article);
    }
}
